# Perceptron Algorithm on the Sonar Dataset
from random import seed
from random import randrange
from csv import reader

# Make a prediction with weights
'''def predict(row, weights):
    activation = weights[0]
    for i in range(len(row) - 1):
        activation += weights[i + 1] * row[i]
    return 1.0 if activation >= 0.0 else 0.0


# Estimate Perceptron weights using stochastic gradient descent
def train_weights(train, l_rate, n_epoch):
    weights = [0.0 for i in range(len(train[0]))]
    for epoch in range(n_epoch):
        sum_error = 0.0
        for row in train:
            prediction = predict(row, weights)
            error = row[-1] - prediction
            sum_error += error ** 2
            weights[0] = weights[0] + l_rate * error
            for i in range(len(row) - 1):
                weights[i + 1] = weights[i + 1] + l_rate * error * row[i]
        print('>epoch=%d, lrate=%.3f, error=%.3f' % (epoch, l_rate, sum_error))
    return weights
'''


# Split a dataset into k folds
def cross_validation_split(dataset, n_folds):
    print(n_folds)
    dataset_split = list()
    dataset_copy = list(dataset)
    fold_size = int(len(dataset) / n_folds)
    for i in range(n_folds):
        fold = list()
        while len(fold) < fold_size:
            index = randrange(len(dataset_copy))
            fold.append(dataset_copy.pop(index))
        dataset_split.append(fold)
    return dataset_split


# Calculate accuracy percentage
def accuracy_metric(actual, predicted):
    correct = 0
    print(len(actual))
    for i in range(len(actual)):
        print('i : ', i)
        if actual[i] == predicted[i]:
            correct += 1
    return correct / float(len(actual)) * 100.0


# Evaluate an algorithm using a cross validation split
def evaluate_algorithm(dataset, algorithm, n_folds, *args):
    folds = cross_validation_split(dataset, n_folds)
    scores = list()
    for fold in folds:
        train_set = list(folds)
        #train_set.remove(fold)
        train_set = sum(train_set, [])
        '''test_set = list()
        for row in fold:
            row_copy = list(row)
            test_set.append(row_copy)
            row_copy[-1] = None'''

        test_set = [[5.465489372, 2.362125076, 2.550537003, None], [5.465489372, 2.362125076, 2.550537003, None]]
        print('test_set : ',test_set)

        predicted = algorithm(train_set, test_set, *args)

        print(len(fold))
        actual = [row[-1] for row in fold]
        for row in fold:
            print(row[-1])
        print('predicted', predicted)
        print('actual : ',actual)
        accuracy = accuracy_metric(actual, predicted)
        scores.append(accuracy)
    return scores


# Make a prediction with weights
def predict(row, weights):
    activation = weights[0]
    for i in range(len(row) - 1):
        #print(weights[i + 1], row[i])
        activation += weights[i + 1] * row[i]
    return 1.0 if activation >= 0.0 else 0.0


# Estimate Perceptron weights using stochastic gradient descent
def train_weights(train, l_rate, n_epoch):
    weights = [0.0 for i in range(len(train[0]))]
    for epoch in range(n_epoch):
        for row in train:
            prediction = predict(row, weights)
            error = row[-1] - prediction
            weights[0] = weights[0] + l_rate * error
            for i in range(len(row) - 1):
                weights[i + 1] = weights[i + 1] + l_rate * error * row[i]
    return weights


# Perceptron Algorithm With Stochastic Gradient Descent
def perceptron(train, test, l_rate, n_epoch):
    predictions = list()
    weights = train_weights(train, l_rate, n_epoch)
    for row in test:

        prediction = predict(row, weights)
        predictions.append(prediction)
    print('predictions = ',predictions)
    return (predictions)

# Calculate weights
dataset = [[2.7810836, 2.550537003, 2.550537003, 0],
           [1.465489372, 2.362125076, 2.550537003, 0],
           [3.396561688, 4.400293529, 2.550537003, 0],
           [1.38807019, 1.850220317, 2.550537003, 0],
           [3.06407232, 3.005305973, 2.550537003, 0],
           [7.627531214, 2.759262235, 2.550537003, 0],
           [5.332441248, 2.088626775, 2.550537003, 0],
           [6.922596716, 1.77106367, 2.550537003, 0],
           [8.675418651, -0.242068655, 2.550537003, 1],
           [7.673756466, 3.508563011, 2.550537003, 1]]
l_rate = 0.8
n_epoch = 10
'''weights = train_weights(dataset, l_rate, n_epoch)
print(weights)'''

n_folds = 5
scores = evaluate_algorithm(dataset, perceptron, n_folds, l_rate, n_epoch)
print('Scores: %s' % scores)
print('Mean Accuracy: %.3f%%' % (sum(scores)/float(len(scores))))