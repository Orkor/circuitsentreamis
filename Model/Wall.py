'''
Modele qui représente un mur
il stoque 2 points (les deux extrémités du mur)
'''

class Wall:

    # Constructeur de la classe
    # Entrée : pt1 et pt2 les deux extrémités du mur
    def __init__(self, pt1, pt2):
        self.pt1 = pt1;
        self.pt2 = pt2;

