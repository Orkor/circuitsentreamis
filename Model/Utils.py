'''
Modele de fonctionalité utiles au programme
Il présente plusieurs fonction
- intersectDist()     Calcule la distance entre A et le point d'intesection des deux segment [AB] [CD]
- intersect()         Teste si il y a une intersection
- rotate()            Fait tourner un point en fonction d'un autre et d'un angle en degrés
- line_intersection() Calcule le point d'intersection de deux ligne
- segmentLength()     Calcule la longueur d'un segment
- getMutation()       Génere une mutation du genome
'''

#Import
from math import pi, sin, cos, sqrt
from Model.Point import Point
import numpy as np

def ccw(A,B,C):
    return (C.y-A.y) * (B.x-A.x) > (B.y-A.y) * (C.x-A.x)

# Calcule la distance entre A et le point d'intesection des deux segment [AB] [CD]
# Entrée : A, B, C et D les extrémité des deux segment [AB] [CD]
def intersectDist(A,B,C,D):
    if ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D):
        sensorLength = segmentLength(A,B)
        detectionLength = segmentLength(A, line_intersection(((A.x, A.y), (B.x, B.y)), ((C.x, C.y), (D.x, D.y))))
        return detectionLength*100/sensorLength /100
    else :
        return 1

# Teste si il y a une intersection
# Entrée : A, B, C et D les extrémité des deux segment [AB] [CD]
# Sortie :  Vrai ou Faux
def intersect(A,B,C,D):
    return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)

# Fait tourner un point en fonction d'un autre et d'un angle en degrés
# Entrée : M point à faire tourner, O centre de rotation et angle angle de rotation en degrés
# Sortie :  x et y nouvelle coordonnées du point M
def rotate (M, O, angle):
    angle = angle * pi / 180
    xM = M.x - O.x
    yM = M.y - O.y
    x = xM * cos(angle) + yM * sin(angle) + O.x
    y = - xM * sin(angle) + yM * cos(angle) + O.y
    return round(x), round(y)

# Calcule le point d'intersection de deux ligne
# Entrée : ligne1 et ligne2 deux segments
# Sortie : point d'intersection entre les deux segments
def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return Point(x, y)

# Calcule la longueur d'un segment
# Entrée : A et B les deux point du segment [AB]
# Sortie : longuer du segment [AB]
def segmentLength(A,B):
    return sqrt((A.x - B.x) ** 2 + (A.y - B.y) ** 2)

# Génere une mutation du genome
# Entrée : genome et coefficient de déviation
# Sortie : la nouvelle motification
def getMutation(genome,stdDeviation):
	return genome + stdDeviation * np.random.randn(8)
