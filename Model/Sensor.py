'''
Modele qui représente un capteur
il stoque 2 points (les deux extrémités de fil de détection)
et la distance de détection 1 si aucune détection
'''


class Sensor:

    # Constructeur de la classe
    # Entrée : pt1 et pt2 les deux extrémités du détecteur
    def __init__(self, pt1, pt2):
        self.pt1 = pt1;
        self.pt2 = pt2;
        self.detect = 1

    # Setter des deux extrémités du détecteur
    # Entrée : pt1 et pt2 les deux extrémités du détecteur
    def setPoint(self, pt1, pt2):
        self.pt1 = pt1;
        self.pt2 = pt2;

    # Getter de tétat de détection
    # Sortie :  Vrai ou Faux en fonction de la distance de détection
    def getDetect(self):
        return True if self.detect < 1 else False