from Config.InterfaceConfig import mlpDataSetFileLeft,mlpDataSetFileRight,mlpDataSetFileForward

class Dataset:

    # Constructeur de la classe
    def __init__(self):
        self.DataSetLeft = []
        self.DataSetRight = []
        self.DataSetForward = []

    # Export des dataSets
    def carExport(self, car, file):
        put = True
        val = '{0},{1},{2},{3}\n'.format(car.sensorList[0].detect, car.sensorList[1].detect, car.sensorList[2].detect, 0 if car.collide else 1)
        myFile = open('Model/IA/DataSet/{0}'.format(file), "r")
        for line in myFile:
            if val == line:
                put = False
        myFile.close()

        if put :
            myFile = open('Model/IA/DataSet/{0}'.format(file), "a")
            myFile.write(val)
            myFile.close()


    # Import des dataset
    def carImport(self):
        myFileLeft = open('Model/IA/DataSet/{0}'.format(mlpDataSetFileLeft), "r")
        myFileRight = open('Model/IA/DataSet/{0}'.format(mlpDataSetFileRight), "r")
        myFileForward = open('Model/IA/DataSet/{0}'.format(mlpDataSetFileForward), "r")

        self.DataSetLeft = []
        for line in myFileLeft :
            part = line.split(',')
            value = []
            for i in range (0,len(part)):
                value.append(float(part[i]))
            self.DataSetLeft.append(value)

        self.DataSetRight = []
        for line in myFileRight:
            part = line.split(',')
            value = []
            for i in range(0, len(part)):
                value.append(float(part[i]))
            self.DataSetRight.append(value)

        self.DataSetForward = []
        for line in myFileForward:
            part = line.split(',')
            value = []
            for i in range(0, len(part)):
                value.append(float(part[i]))
            self.DataSetForward.append(value)

        myFileLeft.close()
        myFileRight.close()
        myFileForward.close()