'''
Modele qui représente les actions de l'IA de machine learning qui simule un robot apprenant
Comportement :
    - Il va dans un premier temps créé X voitures avec un genome propre
    - Il va les faire évoluer dans un circuit
    - Au bout d'un certin temps ou lorsque toute les voiture ce sont crashés il va prendre un
    groupe de voiture.
    - Il va stoquer la meilleure voiture(distance) dans un document puis avec le groupe
    précedement selectionné, créer une nouvelle génération en reproduisant le processus
'''

# import
import numpy as np
from Config.InterfaceConfig import mlpDataSetFileLeft, mlpDataSetFileRight, mlpDataSetFileForward, mlpDataSetGeneration, loadSave, genSelect, IaGenerationSize, IaMutation, IaDefaultGenerationTime, interfaceType,trackMode
from operator import attrgetter
from Model.Car import Car
from Model.Utils import getMutation
from Model.IA.DataSet.Dataset import Dataset
import copy

class IA2:

    # Constructeur de la classe
    def __init__(self):
        self.simulate = True        # Autorise la simulation automatique
        self.bestCar = []           # Liste des meilleure voiture
        self.t = 0                  # Temps
        self.dt = 0.1               # Delta temps
        self.crash = 0              # Nombre de voiture crashée
        self.n = IaGenerationSize   # Nombre de voiture dans la génération
        self.generation = 1         # Numéros de la génération
        self.bestScore = 0          # Meilleure distance parcourue
        self.bestGen = 0            # Meilleure génération
        self.carlist = []           # Liste de voitures
        self.trackList = []         # Liste de circuits
        self.trackId = 0            # ID du circuits
        if mlpDataSetGeneration:
            self.Dataset = Dataset()    # Création du data set si la config est active

    # Chargement du cicuit
    # Entrée : circuit à parcourir
    def loadTrack(self, track):
        self.track = track

    # Chargement des cicuits
    # Entrée : liste des circuits à parcourir
    def loadTrackMultiple(self, tracks):
        self.trackList = tracks
        self.trackId = 0
        self.loadTrack(self.trackList[self.trackId])

    # Chargement de la voiture
    # Entrée : voiture modèle
    def loadCar(self, car):
        self.carlist = self.randomCarList(IaGenerationSize, copy.copy(self.track.startPoint)) #Genere un liste aléatoire de voiture

    # Fonction qui simule l'intéligence de la voiture
    # Comportement voir détail du Modele IA2
    # Sortie : Si nouvelle génération et si nouveau circuit
    def inteligence(self):
        newgen = False
        changeTrack = False

        #Déplace les voitures si elle n'a pas rencontré un mur
        for car in self.carlist:
            if (not car.collide):
                car = self.move(car)
                if (car.collide):
                    if mlpDataSetGeneration:
                        self.Dataset.carExport(car, mlpDataSetFileRight)
                        self.Dataset.carExport(car, mlpDataSetFileLeft)
                        self.Dataset.carExport(car, mlpDataSetFileForward)
                    self.crash += 1
                else:
                    car.ai()

        #Créé une nouvelle génération ou change le circuit
        if interfaceType != 'LOAD':
            if (len(self.carlist) > 0):
                self.t += self.dt       # Fait evoluer le temps d'execution
                if (self.t > IaDefaultGenerationTime or self.crash >= IaGenerationSize):
                    # Si nous somme à cours de temps ou que toute les voitures se sont crashés
                    if trackMode != 'MULTI': # Si pas mode Multi circuit
                        self.newGen()   # Nouvelle génération
                        newgen = True
                    else : # Si mode Multi circuit
                        # Verification que tous les circuit ont été parcourus
                        # Si oui : reset et nouvelle génération
                        # Si non : reset
                        if(self.trackId+1 < len(self.trackList)):
                            self.trackId += 1
                            self.loadTrack(self.trackList[self.trackId])

                            self.t, self.crash = 0, 0
                            for car in self.carlist:
                                car.collide = False
                                car.center = copy.copy(self.track.startPoint)
                        else:
                            self.trackId = 0
                            self.loadTrack(self.trackList[self.trackId])
                            self.newGen()
                            newgen = True
                        print('Track id : ',self.trackId)
                        changeTrack = True


        return newgen, changeTrack

    # Fonction qui gere la sauvegarde de la l'ancienne génération et qui créé la suivante
    def newGen(self):
        self.t, self.crash = 0, 0
        best = self.getNextGen() # Création de la nouvelle génération
        #Mise à jour des données de l'affichage
        if (best > self.bestScore):
            self.bestScore = best
            self.bestGen = self.generation

        self.bestCar[len(self.bestCar) - 1].carExport(self.generation) # Export de meilleur
        print('Generation(', self.generation, ') -> ', self.bestCar[len(self.bestCar) - 1].distance,
              " || Best : gen {0} dist {1}".format(self.bestGen, format(self.bestScore, '.2f')))
        self.generation += 1    # Incrémentation de la génération

    # Fonction qui gere une liste aléatoires de voiture ou chargée de voiture
    # Entrée : n le nombre de voiture dans la génération, center position initiale de la voiture
    # Sortie : Liste des nouvelle voiture
    def randomCarList(self, n, center):
        cars = []
        if interfaceType=='LOAD' :
            n=1
        for i in range(n):
            cars.append(Car(center, 0, i))
            # Generation en fonction de la génération chargée
            if loadSave or interfaceType=='LOAD' :
                self.generation = genSelect
                cars[i].carImport(genSelect)
            # Generation aléatoire
            else :
                cars[-1].genome = 0.1 * np.random.randn(8)
        return cars

    # Déplace la voiture
    # Entrée : Voiture à déplacer
    # Sortie : Voiture déplacée
    def move(self, car):
         #Roation
        if (car.turn > 0.03):
            if mlpDataSetGeneration:
                self.Dataset.carExport(car,mlpDataSetFileRight)
            car.right()
        elif (car.turn < 0.03):
            if mlpDataSetGeneration:
                self.Dataset.carExport(car,mlpDataSetFileLeft)
            car.left()

        # Mouvement
        if (car.power > 1):
            car.power = 1.0
        elif (car.power < -1):
            car.power = -1.0

        car.speed += 30 * car.power * self.dt - 0.02 * car.speed
        if car.speed < 0.0:
            car.speed = 0.0
        else :
            if mlpDataSetGeneration:
                self.Dataset.carExport(car,mlpDataSetFileForward)
        car.forward()
        car.distance += car.speed*self.dt

        car.setSensorDetection(self.track.checkColideSensor(car)) # MAJ état des détecteurs
        car.collide = self.track.checkColideCar(car) # MAJ des colisions

        return car

    # Création de la nouvelle génération
    # Sortie : Renvoi la meilleure distance
    def getNextGen(self):
        best = sorted(self.carlist, key=attrgetter('distance')) #Tris les voiture
        best.reverse()

        child = self.ChildGen(IaGenerationSize) #Genere la liste des enfant
        newCar = []
        id = 0
        #Créer les nouvelles voitures
        for i in child:
            newCar.append(Car(copy.copy(self.track.startPoint),0, id))
            newCar[-1].genome = getMutation(best[i].genome, IaMutation)
            id += 1
        self.carlist = newCar
        self.bestCar.append(best[0])
        return best[0].distance

    # Création de la liste des enfants (voiture à séléctionner pour préparer la génération suivante)
    # Entrée : taille de la génération
    # Sortie : liste des voiture à séléctionner
    def ChildGen(self, gendim):
        listChild = []
        number = 0
        numberMax = 0
        for i in range(0, gendim):
            listChild.append(number)
            listChild.sort()
            if (numberMax == number):
                number = 0
                numberMax += 1
            else:
                number += 1

        return listChild