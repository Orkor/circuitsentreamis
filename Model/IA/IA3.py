'''
Modele qui représente les actions de l'IA de machine learning qui simule un robot apprenant
Comportement :
    - Il va dans un premier temps créé X voitures avec un genome propre
    - Il va les faire évoluer dans un circuit
    - Au bout d'un certin temps ou lorsque toute les voiture ce sont crashés il va prendre un
    groupe de voiture.
    - Il va stoquer la meilleure voiture(distance) dans un document puis avec le groupe
    précedement selectionné, créer une nouvelle génération en reproduisant le processus
'''

# import
import numpy as np
from Config.InterfaceConfig import mlpRate, mlpEpoch
from operator import attrgetter
from Model.Car import Car
from Model.Utils import getMutation
from random import randrange
from Model.IA.DataSet.Dataset import Dataset
import copy

class IA3:

    # Constructeur de la classe
    def __init__(self):
        self.simulate = True        # Autorise la simulation automatique
        self.t = 0                  # Temps
        self.dt = 0.1               # Delta temps
        self.carlist = []           # Liste de voitures
        self.trackList = []         # Liste de circuits
        self.trackId = 0            # ID du circuits
        self.dataset = Dataset()    # Chargement du data set
        self.dataset.carImport()

        # Chargement du cicuit
    # Entrée : circuit à parcourir
    def loadTrack(self, track):
        self.track = track

    # Chargement de la voiture
    # Entrée : voiture modèle
    def loadCar(self, car):
        self.carlist.append(car)

    # Fonction qui simule l'intéligence de la voiture
    # Comportement voir détail du Modele IA2
    # Sortie : Si nouvelle génération et si nouveau circuit
    def inteligence(self):
        newgen = False
        changeTrack = False

        print(self.dataset.DataSetForward)

        scores = self.evaluate_algorithm(self.dataset.DataSetForward, self.perceptron, mlpRate, mlpEpoch)
        print('Scores: %s' % scores)
        print('Mean Accuracy: %.3f%%' % (sum(scores) / float(len(scores))))

        #Déplace les voitures si elle n'a pas rencontré un mur
        '''for car in self.carlist:
            if (not car.collide):
                car = self.move(car)
                if (car.collide):
                    self.crash += 1
                else:
                    car.ai()'''

        return False, False


    # Déplace la voiture
    # Entrée : Voiture à déplacer
    # Sortie : Voiture déplacée
    def move(self, car):
         #Roation
        if (car.turn > 0.03):
            car.right()
        elif (car.turn < 0.03):
            car.left()

        # Mouvement
        if (car.power > 1):
            car.power = 1.0
        elif (car.power < -1):
            car.power = -1.0

        car.speed += 30 * car.power * self.dt - 0.02 * car.speed
        if car.speed < 0.0:
            car.speed = 0.0
        car.forward()
        car.distance += car.speed*self.dt

        car.setSensorDetection(self.track.checkColideSensor(car)) # MAJ état des détecteurs
        car.collide = self.track.checkColideCar(car) # MAJ des colisions

        return car



#----------------------------------------------------------------------------------------
    # Split a dataset into k folds
    def cross_validation_split(self, dataset):
        n_folds = (int)(len(dataset)/2)
        print(n_folds)
        dataset_split = list()
        dataset_copy = list(dataset)
        fold_size = int(len(dataset) / n_folds)
        for i in range(n_folds):
            fold = list()
            while len(fold) < fold_size:
                index = randrange(len(dataset_copy))
                fold.append(dataset_copy.pop(index))
            dataset_split.append(fold)
        return dataset_split


    # Calculate accuracy percentage
    def accuracy_metric(self, actual, predicted):
        correct = 0
        for i in range(len(actual)):
            if actual[i] == predicted[i]:
                correct += 1
        return correct / float(len(actual)) * 100.0


    # Evaluate an algorithm using a cross validation split
    def evaluate_algorithm(self, dataset, algorithm, *args):
        folds = self.cross_validation_split(dataset)
        print('folds : ',folds)
        scores = list()
        count = 0
        for fold in folds:
            train_set = list(folds)
            train_set = sum(train_set, [])

            valset = list()
            for sensor in self.carlist[0].sensorList:
                valset.append(sensor.detect)
            valset.append(None)

            test_set = list()
            test_set.append(valset)
            test_set.append(valset)
            #print('test_set : ', test_set)

            predicted = algorithm(train_set, test_set, *args)
            actual = [row[-1] for row in fold]
            accuracy = self.accuracy_metric(actual, predicted)
            scores.append(accuracy)
            count+=1
            print(count)
        return scores


    # Make a prediction with weights
    def predict(self, row, weights):
        activation = weights[0]
        for i in range(len(row) - 1):
            #print(weights[i + 1],row[i])
            activation += weights[i + 1] * row[i]
        return 1.0 if activation >= 0.0 else 0.0


    # Estimate Perceptron weights using stochastic gradient descent
    def train_weights(self, train, l_rate, n_epoch):
        weights = [0.0 for i in range(len(train[0]))]
        for epoch in range(n_epoch):
            for row in train:
                prediction = self.predict(row, weights)
                error = row[-1] - prediction
                weights[0] = weights[0] + l_rate * error
                for i in range(len(row) - 1):
                    weights[i + 1] = weights[i + 1] + l_rate * error * row[i]
        return weights


    # Perceptron Algorithm With Stochastic Gradient Descent
    def perceptron(self, train, test, l_rate, n_epoch):
        predictions = list()
        weights = self.train_weights(train, l_rate, n_epoch)
        for row in test:
            prediction = self.predict(row, weights)
            predictions.append(prediction)
        return (predictions)