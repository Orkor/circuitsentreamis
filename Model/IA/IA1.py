'''
Modele qui représente les actions de l'IA Simple qui simule un robot moustache
Comportement :
    Si le capteur droit détécte un mur la voiture va tourner vers la gauche
    Si Le capteur ne détécte rien devant lui la voiture va avaner
    Sinon la voiture va tourner vers la droite
'''

class IA1:

    # Constructeur de la classe
    def __init__(self):
        self.simulate = True    # Autorise la simulation automatique
        self.carlist = []       # Liste de voiture ici seulement une

    # Chargement du cicuit
    # Entrée : circuit à parcourir
    def loadTrack(self, track):
        self.track = track

    # Chargement de la voiture
    # Entrée : voiture modèle
    def loadCar(self, car):
        self.carlist.append(car)

    # Fonction qui simule l'intéligence de la voiture
    # Comportement voir détail du Modele IA1
    # Sortie : Faux et Faux
    def inteligence(self):
        if not self.carlist[0].sensorList[0].getDetect() and not self.carlist[0].sensorList[1].getDetect() and  not self.carlist[0].sensorList[2].getDetect():
            self.carlist[0].forward()
        elif self.carlist[0].sensorList[2].getDetect() :
            self.carlist[0].left()
        else:
            self.carlist[0].right()

        self.carlist[0].setSensorDetection(self.track.checkColideSensor(self.carlist[0])) # MAJ état des détecteurs
        self.carlist[0].collide = self.track.checkColideCar(self.carlist[0]) # MAJ des colisions

        return False, False

