'''
Modele qui représente les actions humaines
il stoque une voiture
'''

class User:

    # Constructeur de la classe
    def __init__(self):
        self.simulate = False   # Bloque la simulation automatique
        self.carlist = []       # Liste de voiture ici seulement une

    # Chargement du cicuit
    # Entrée : circuit à parcourir
    def loadTrack(self, track):
        self.track = track

    # Chargement de la voiture
    # Entrée : voiture modèle
    def loadCar(self, car):
        self.carlist.append(car)

    # Mise à jour des données de la voiture
    def update(self):
        self.carlist[0].setSensorDetection(self.track.checkColideSensor(self.carlist[0])) # MAJ état des détecteurs
        self.carlist[0].collide = self.track.checkColideCar(self.carlist[0]) # MAJ des colisions

    # Movement à gauche
    def left(self):
        self.carlist[0].left()
        self.update()

    # Movement à droite
    def right(self):
        self.carlist[0].right()
        self.update()

    # Movement vers l'avant
    def forward(self):
        self.carlist[0].forward()
        self.update()