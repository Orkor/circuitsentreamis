'''
Modele qui représente les circuit
il stoque la liste des murs du circuit, le point de départ et une repere exterieur au circuit
'''

#Import
from Model.Wall import Wall
from Model.Point import Point
from Model.Utils import intersect,intersectDist
from Config.InterfaceConfig import TrackLoadFile

class Track:

    # Constructeur de la classe
    # Entrée : type de circuit(1,2,3,-1)
    def __init__(self,loadFile):
        self.list = []          # Liste des murs
        self.listRepere = []    # Liste des point de repere exterieur au circuit
        self.loadFile = loadFile
        self.trackImport()

    # Verifi si une voiture a rencontré un mur
    # Entrée : La voiture à tester
    # Sortie : Si la voiture a rencontré un mur on renvoir Vrai sinon faux
    # Fonctionnement : On prend un point de la voiture et un repère en dehors du circuit. Si ce segment coupe
    # un mur on incrémente une variable initalisé à 0. On répéte ce processus pour chaque mur du circuit.
    # A la fin on regarde si la variable est un chiffre pair ou impair. s'il est pair c'est que la voiture
    # n'a pas rencontré de mur sinon oui. On répéte cet action pourles deux coint avant de la voiture.
    def checkColideCar (self,car):
        detect = False
        for i in range(1,len(car.sensorList)):
            for pointRep in self.listRepere :
                nbDetect = 0
                for wall in self.list:
                    if intersect(car.sensorList[i].pt1,pointRep,wall.pt1,wall.pt2):
                        nbDetect += 1
                detect = True if nbDetect%2 == 0 else detect
        return detect

    # Verifi si les capteur d'une voiture détécte un  mur
    # Entrée : La voiture à tester
    # Sortie : liste des distance de détection de chaque capteur
    # Fonctionnement : On verifi pour chaque capteur si il n'a pas rencontré un mur dans la liste puis on
    # met la distance de ce dernier dans un tableau
    def checkColideSensor (self,car):
        detectList = []
        for sensor in car.sensorList:
            detect = 1
            for wall in self.list:
                newval = intersectDist(sensor.pt1,sensor.pt2,wall.pt1,wall.pt2)
                detect = newval if newval < detect else detect
            detectList.append(detect)
        return detectList

    # Importation du circuit stoqué dans le chichié défini en config
    def trackImport(self):
        self.listRepere.append(Point(0, 0))
        myFile = open("Tracks/{0}".format(self.loadFile), "r")       # Ouvre le fichier en lecture
        for line in myFile:                     # Pour chaque ligne on parse les données
            points = line.split('#')            # Le '#' est la séparation entre deux points pour un meme mur
            if len(points) < 2 :                # Si il n'y a pas de '#' c'est que la ligne correspond au point de départ et de repère
                points = line.split('||')       # le '||' est la séparation entre repère et point de départ
                pt1 = points[0].split(',')      # le ',' est la séparation les coordonnées d'un meme point
                pt2 = points[1].split(',')
                self.listRepere.append(Point(int(pt1[0]), int(pt1[1])))
                self.startPoint = Point(int(pt2[0]), int(pt2[1]))
            else:
                pt1 = points[0].split(',')
                pt2 = points[1].split(',')
                self.list.append(Wall(Point(int(pt1[0]), int(pt1[1])), Point(int(pt2[0]), int(pt2[1]))))
        myFile.close()                          # Ferme le fichier


