'''
Modele qui représente un point
il stoque les coordonées X et Y d'un point
'''

class Point:

    # Variable locale à la classe
    x = 0
    y = 0

    # Constructeur de la classe
    # Entrée : x et y les deux coordonées d'un point
    def __init__(self, x, y):
        self.x = x
        self.y = y

    # Getter des deux coordonnées
    def getXY(self):
        return self.x, self.y

    # Setter des deux coordonées d'un
    # Entrée : x et y les deux coordonées d'un point
    def setXY(self, x, y):
        self.x = x
        self.y = y

    # Afficchage des coordonnées du point
    def toString(self):
        print ('(', self.x, ',', self.y, ')')

    # OLD cette fonction n'est plus utilisée
    def getToString(self):
        return self.x, self.y
