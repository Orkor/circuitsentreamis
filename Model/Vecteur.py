'''
OLD cette classe n'est plus utilisée
'''

from math import sqrt,pow

class Vecteur:
    def __init__(self, pointA, pointB):
        self.x = pointB.x - pointA.x
        self.y = pointB.y - pointA.y

    def vecto(self, vecteur):
        return (self.x * vecteur.y - self.y * vecteur.x)

    def norme(self):
        return sqrt(self.x * self.x + self.y * self.y)