'''
Modele qui représente une voiture
'''

#Import
from Model.Point import Point
from Model.Sensor import Sensor
from Config.InterfaceConfig import carLoadFile, carSaveFile, carLength, carWidth, sensorDetectionRange, carRotateOffset, carMoveOffset, sensorAngle
from Model.Utils import rotate

class Car:

    # Constructeur de la classe
    # Entrée : Centre de la voiture, vitesse initalle et son id
    def __init__(self, center, speed, id):
        self.center = center    # Centre de la voiture
        self.speed = speed      # Vitesse de la voiture
        self.distance = 0       # Distance parcourue
        self.genome = []        # Génome de la voiture
        self.sensorList = []    # Liste des capteur de la voiture
        self.collide = False    # Voiture crashée
        self.a = 1.5            # a
        self.turn = 0           # Coefficient de rotation
        self.power = 0          # Puissance de la voiture

        self.angle = 0          # Angle de la voiture

        self.defineBorder()     # Dététermination des extrémité de la voiture
        self.defineSensor()     # Définition des capteur
        self.id = id            # ID de la voiture

    # Dététermination des extrémité de la voiture
    def defineBorder(self):
        self.x1 = self.center.x - carLength / 2
        self.x2 = self.center.x + carLength / 2
        self.y1 = self.center.y
        self.y2 = self.center.y

    # Rotation à gauche si voiture non crashée
    def left(self):
        if not self.collide:
            self.angle = self.angle + carRotateOffset if self.angle + carRotateOffset< 360 else self.angle + carRotateOffset - 360
            self.rotate()

    # Rotation à droite si voiture non crashée
    def right(self):
        if not self.collide:
            self.angle = self.angle - carRotateOffset if self.angle - carRotateOffset > -1 else self.angle - carRotateOffset + 360
            self.rotate()

    # Mouvement vers l'avant si voiture non crashée
    def forward(self):
        if not self.collide:
            deltax = self.x2 - self.center.x
            deltay = self.y2 - self.center.y
            if (deltax == 0):
                self.center.y = self.center.y - self.speed if deltay < 0 else self.center.y + self.speed
            elif(deltay == 0):
                self.center.x = self.center.x - self.speed if deltax < 0 else self.center.x + self.speed

            elif(abs(deltax)>= abs(deltay)):
                m = (deltay) / (deltax)
                self.center.x = self.center.x - self.speed if deltax < 0 else self.center.x + self.speed
                self.center.y = self.center.y - (abs(m) * self.speed) if deltay < 0 else self.center.y + (abs(m) * self.speed)
            else :
                m = (deltay) / (deltax)
                self.center.y = self.center.y - self.speed if deltay < 0 else self.center.y + self.speed
                self.center.x = self.center.x - (self.speed / abs(m)) if deltax < 0 else self.center.x + (self.speed / abs(m))
            self.rotate()


    # Rotation de chaqu'un des point de la voiture et des capteur
    def rotate(self):

        self.defineBorder()
        self.sensorList[0].setPoint(Point(carLength / 2 + self.center.x, self.center.y), Point(carLength / 2 + self.center.x + sensorDetectionRange, self.center.y))
        self.sensorList[1].setPoint(Point(carLength / 2 + self.center.x, self.center.y - carWidth / 2), Point(carLength / 2 + self.center.x + sensorDetectionRange, self.center.y - sensorAngle - carWidth / 2))
        self.sensorList[2].setPoint(Point(carLength / 2 + self.center.x, self.center.y + carWidth / 2), Point(carLength / 2 + self.center.x + sensorDetectionRange, self.center.y + sensorAngle + carWidth / 2))

        self.x1, self.y1 = rotate(Point(self.x1, self.y1), self.center, self.angle)
        self.x2, self.y2 = rotate(Point(self.x2, self.y2), self.center, self.angle)

        for sensor in self.sensorList :
            sensor.pt1.x, sensor.pt1.y = rotate(sensor.pt1, self.center, self.angle)
            sensor.pt2.x, sensor.pt2.y = rotate(sensor.pt2, self.center, self.angle)


    # Ajout et création des capteur à la liste
    def defineSensor(self):
        self.sensorList.append(Sensor(Point(carLength / 2 + self.center.x, self.center.y), Point(carLength / 2 + self.center.x + sensorDetectionRange, self.center.y)))
        self.sensorList.append(Sensor(Point(carLength / 2 + self.center.x, self.center.y - carWidth / 2), Point(carLength / 2 + self.center.x + sensorDetectionRange, self.center.y - sensorAngle - carWidth / 2)))
        self.sensorList.append(Sensor(Point(carLength / 2 + self.center.x, self.center.y + carWidth / 2), Point(carLength / 2 + self.center.x + sensorDetectionRange, self.center.y + sensorAngle + carWidth / 2)))

    # Atribution des détection à chaque capteur
    # Entrée : liste des distance de détection des capteur
    def setSensorDetection(self,listDetect):
        for i in range(0, len(self.sensorList)):
            self.sensorList[i].detect = listDetect[i]

    # Mise à jour de la puissance et du coéficient de rotation de la voiture en fonction du
    # genome et de la valeur des capteur
    def ai(self):
        genome = self.genome
        self.power = genome[0] * self.sensorList[0].detect + genome[1] * self.sensorList[1].detect + genome[2] * self.sensorList[2].detect + genome[3]
        self.turn = genome[4] / 10 * self.sensorList[0].detect + genome[5] / 10 * self.sensorList[1].detect + genome[6] / 10 * self.sensorList[2].detect + genome[7] / 10

    # Export de la meilleure voiture et de son génome dans  le fichier car save
    def carExport(self, generation):
        myFile = open(carSaveFile, "a")
        generationText = self.genome[0]
        for val in range(1,len(self.genome)):
            generationText = '{0},{1}'.format(generationText, self.genome[val])
        myFile.write("{0}#{1}#{2}\n".format(generation, generationText, self.distance))
        myFile.close()

    # Import d'une voiture
    # Entrée : génération voulue
    def carImport(self, generation):
        myFile = open(carLoadFile, "r")
        newGenome = []
        for line in myFile :
            if '{}#'.format(generation) in line:
                part = line.split('#')
                newGenomeText = part[1].split(",")
                for i in range (0,len(newGenomeText)):
                    newGenome.append(float(newGenomeText[i]))
        self.genome = newGenome
        myFile.close()

