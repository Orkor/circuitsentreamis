'''
View qui représente une voiture graphiquement
'''

#Import
from Config.InterfaceConfig import carWidth, carColorCollide
from view.SensorModel import SensorModel
import random

class CarModel:

    sensorList = []

    # Constructeur de la classe
    # Entrée : voiture associé et canvas ou cera affiché la voiture
    def __init__(self, car, can):
        # Sauvegarde des variable d'entrées
        self.can = can
        self.car = car

        self.color = self.colorRand()   #Deffinition de la couleur de la voiture
        # Construction de l'élémént graphique
        self.carM = self.can.create_line(self.car.x1, self.car.y1, self.car.x2, self.car.y2, width=carWidth, fill=self.color , tags='test')
        self.paintSensor()

    # Generation couleurs aléatoire
    # Sortie : hexa de la couleur
    def colorRand(self):
        de = ("%02x" % random.randint(0, 255))
        re = ("%02x" % random.randint(0, 255))
        we = ("%02x" % random.randint(0, 255))
        ge = "#"
        return ge + de + re + we

    # Créé les elements graphique représentant les capteurs si ils sont visible dans la config
    def paintSensor (self):
        if (SensorModel.visible) :
            for sensor in self.car.sensorList:
                self.sensorList.append(SensorModel(sensor,self.can))

    # Fonction qui recharge l'élémént graphique
    def repaint(self):
        self.can.coords(self.carM, self.car.x1, self.car.y1, self.car.x2, self.car.y2)
        if (self.car.collide):  # Change la couleur du capteur en fonction de son état
            self.can.itemconfig(self.carM, fill=carColorCollide)
        else:
            self.can.itemconfig(self.carM, fill=self.color)

        if (SensorModel.visible):   # Recharge les capteurs si ils sont visible dans la config
            for sensorModel in self.sensorList:
                sensorModel.repaint()

    # Fonction qui Supprimme l'élémént graphique
    def deleteCar(self):
        for sensorM in self.sensorList:
            sensorM.deleteSensor()
        self.can.delete(self.carM)







