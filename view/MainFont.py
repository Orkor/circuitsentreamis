'''
View main utilisé pour l'affichage graphique dans une fenetre
'''

#import
from tkinter import *
from Config.InterfaceConfig import moveForward, rotateLeft, rotateRight, simulationSpeed, fenLength, fenWidth, carVisible, interfaceType, trackMode
from view.CarModel import CarModel
from view.WallModel import WallModel
from view.SensorModel import SensorModel

class MainFont:

    val =False

    # Constructeur de la classe
    # Entrée : Entité associée et nom de la fenetre
    def __init__(self, name, ia):
        self.c1 = []    # Car model list
        self.track = [] # Circuit list
        self.ia = ia    # Entité de controle

        self.fenetre = Tk()         # Création de la fenetre
        self.fenetre.title(name)    # Nomage de la fenetre
        self.can = Canvas(self.fenetre, width=fenLength, height=fenWidth, bg="black")   # Création du support des element graphique
        self.can.pack()
        self.loadTrack()            # Chargement des Circuits
        self.loadCar()              # Chargement des voiture

        # Checkbox d'affichage ou non des capteur
        self.checkVisibleSensor = Checkbutton(self.fenetre, text="Visible Sensor", command=self.toggleSensor, variable=self.val)
        self.checkVisibleSensor.pack()

        # Pour le mode machine learning chargement des valeur numérique de suivi des generations
        if (interfaceType == 'IA2'):
            if trackMode == 'MULTI' :
                self.trackTest = self.can.create_text(20, fenWidth * 35 / 40, anchor='nw', fill='white', font="Arial 12", text="Track : none")
            self.crashText = self.can.create_text(20, fenWidth * 36 / 40, anchor='nw', fill='white', font="Arial 12", text="Crash : none")
            self.tmpText = self.can.create_text(20, fenWidth * 37 / 40, anchor ='nw', fill='white', font="Arial 12", text="Temps : none")
            self.genText = self.can.create_text(20, fenWidth * 38 / 40, anchor ='nw', fill='white', font="Arial 12", text="Gen : none")
            self.bestText = self.can.create_text(20, fenWidth * 39 / 40, anchor ='nw', fill='white', font="Arial 12", text="Best : gen none dist none")

    # Fonction qui démmarre la simulation
    def start(self):
        if self.ia.simulate: # Si tous les mode sauf USER
            self.simulator()
        else:
            self.listener()
        self.fenetre.mainloop()

    # Toggle visible ou non des capteurs
    def toggleSensor(self):
        SensorModel.visible = not SensorModel.visible

    # Création du circuits
    def loadTrack(self):
        if len(self.track)>0 :  # Suppression du circuit précédent si il existe
            for wall in self.track:
                wall.deleteWall()
        for wall in self.ia.track.list: # Creation du nouveau circuit
            self.track.append(WallModel(wall, self.can))

    # Creation des voiture sur le circuit
    def loadCar(self):
        if carVisible :
            for car in self.ia.carlist:
                self.c1.append(CarModel(car, self.can))

    # Bind des touches de la config à chaque action (gauche, droite, avant)
    def listener(self):
        self.fenetre.bind(rotateLeft, self.leftKey)
        self.fenetre.bind(rotateRight, self.rightKey)
        self.fenetre.bind(moveForward, self.upKey)

    # Evenement pour les mouvement utilisateur
    def leftKey(self, event):
        self.ia.left()
        self.update(False)

    def rightKey(self, event):
        self.ia.right()
        self.update(False)

    def upKey(self, event):
        self.ia.forward()
        self.update(False)
    # ----------------------------------------

    # Mise à jour de l'affichage
    # Entrée : Valeur booleène pour savoir si c'est une nouvelle génération
    def update(self,newgen):
        if newgen:
            # Reset des voiture
            if carVisible:
                for carM in self.c1:
                    carM.deleteCar()
                self.c1 = []
                for car in self.ia.carlist:
                    self.c1.append(CarModel(car, self.can))
        else:
            if carVisible:
                for car in self.ia.carlist:
                    self.c1[car.id].repaint()
        if (interfaceType == 'IA2'):
            if trackMode == 'MULTI' :
                self.can.itemconfig(self.trackTest, text="Track : {0}/{1}".format(self.ia.trackId+1,len(self.ia.trackList)))
            self.can.itemconfig(self.crashText, text="Crash : {0}/{1}".format(self.ia.crash,len(self.ia.carlist)))
            self.can.itemconfig(self.tmpText, text="Temps : {0}".format(format(self.ia.t,'.2f')))
            self.can.itemconfig(self.genText, text="Gen : {0}".format(self.ia.generation))
            self.can.itemconfig( self.bestText, text="Best : gen {0} dist {1}".format(self.ia.bestGen,format(self.ia.bestScore,'.2f')))

    def simulator(self):
        newgen,changeTrack = self.ia.inteligence()
        if(changeTrack):
            print('------------------------------------------------------------------------------------------')
            self.loadTrack()
        self.update(newgen)
        self.fenetre.after(simulationSpeed, self.simulator)


