'''
View qui représente un capteur graphiquement
'''

#Import
from Config.InterfaceConfig import sensorWidth, sensorColor, sensorColorCollide, sensorVisible


class SensorModel:

    visible = sensorVisible

    # Constructeur de la classe
    # Entrée : capteur associé et canvas ou cera affiché le capteur
    def __init__(self, sensor, can):
        # Sauvegarde des variable d'entrées
        self.can = can
        self.sensor = sensor

        # Construction de l'élémént graphique
        self.sensorM = self.can.create_line(self.sensor.pt1.x, self.sensor.pt1.y, self.sensor.pt2.x, self.sensor.pt2.y, width=sensorWidth, fill=sensorColor)

    # Fonction qui recharge l'élémént graphique
    def repaint(self):
        self.can.coords(self.sensorM, self.sensor.pt1.x, self.sensor.pt1.y, self.sensor.pt2.x, self.sensor.pt2.y)
        if (self.sensor.getDetect()):   # Change la couleur du capteur en fonction de son état
            self.can.itemconfig(self.sensorM, fill=sensorColorCollide)
        else:
            self.can.itemconfig(self.sensorM, fill=sensorColor)

    # Fonction qui Supprimme l'élémént graphique
    def deleteSensor(self):
        self.can.delete(self.sensorM)