'''
View qui représente un mur graphiquement
'''

#Import
from Config.InterfaceConfig import wallWidth,wallColor

class WallModel:
    # Constructeur de la classe
    # Entrée : mur associé et canvas ou cera affiché le mur
    def __init__(self, wall, can):
        # Coordonées du mur
        self.x1 = wall.pt1.x
        self.x2 = wall.pt2.x
        self.y1 = wall.pt1.y
        self.y2 = wall.pt2.y

        # Sauvegarde des variable d'entrées
        self.can = can
        self.wall = wall

        # Construction de l'élémént graphique
        self.wallM = can.create_line(self.x1, self.y1, self.x2, self.y2, width=wallWidth, fill=wallColor)

    # Fonction qui recharge l'élémént graphique
    def repaint(self):
        self.can.coords(self.wallM, self.x1, self.y1, self.x2, self.y2)

    # Fonction qui Supprimme l'élémént graphique
    def deleteWall(self):
        self.can.delete(self.wallM)