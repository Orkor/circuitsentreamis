'''
Run.py est l'executable du programme circuit
Dans ce projet nous avons utilisé le modéle de conception MVC (Modele, Vue, Controleur)
Il fait appel à la classe "MainController" le controleur principal du projet
'''
from controller.MainController import MainController

MainController()