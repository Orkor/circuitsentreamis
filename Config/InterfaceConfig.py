'''
Ce fichier regroupe les configurations de l'interface graphique et de la simulation
Pour la gestion des circuit rendes vou dans Controller/MainController
'''

###################################### [INTERFACE] #######################################
# Configuration de la fenetre ------------------------------------------------------------
fenLength = 900             # Longueur de la fenetre
fenWidth = 900              # Largeur de la fenetre
loadTK = True              # Graghique ou non (uniquement pour le mode IA2)

# Configuration des voitures -------------------------------------------------------------
carVisible = True           # Voiture visible
carMoveOffset = 1           # A chaque movement de combien de pixel la voiture ce déplace
carRotateOffset = 5         # A chaque rotation de combien de pixel la voiture tourne
carLength = 40              # Longeur de la voiture
carWidth = 20               # Largeur de la voiture
carColorCollide = 'red'     # Couleur de la voiture si colision

# Configuration des murs -----------------------------------------------------------------
wallWidth = 1               # Largeur des murs
wallColor = 'gray'          # Couleur des murs

# Configuration des capteurs -------------------------------------------------------------
sensorVisible = False       # Capteur visible (Si visible ralentissement en mode graphique)
sensorWidth = 1             # Largeur du capteur
sensorDetectionRange = 60   # Distance de détection
sensorAngle = 30            # Angle de détection
sensorColor = 'green'       # Couleur du capteur sans détection
sensorColorCollide = 'red'  # Couleur du capteur avec détéction


###################################### [SIMULATION] ######################################
# Confuguration de l'interface de simulation ---------------------------------------------
# USER => Commande utilisateur au clavier avec les touches configurées ci aprés
# IA1  => IA qui suit le modèle du robot moustache voir Modele/IA/IA1 pour plus de détails
# IA2  => IA qui apprand de génération en génération voir Modele/IA/IA2 pour plus de détails
# IA3  => IA MLP (Non Fonctionnel)
# LOAD => Charge la génération choisie dans 'genSelect' depuis le fichié 'carLoadFile'
interfaceType = 'IA2'               # Mode de l'interface (USER,IA1,IA2,LOAD)
loadSave = True                    # Chargement d'une géréation précedente (uniquement pour le mode IA2)
genSelect = 424                     # Numéros de la génération choisie il est chargé depuis le fichié 'carLoadFile'(voir ci apres)
carSaveFile = "GenomeSave.txt"      # Fichier où sont sauvegardé les melleurs genome de chaque génération (uniquement pour le mode IA2) attention ce fichier est reinitialisé à chaque lancement
carLoadFile = "GenomeSaveval.txt"   # Fichier ou est stoqué la génération à charger
simulationSpeed = 1                # Vitesse de simulation

# Configuration intélligence artificielle ------------------------------------------------
IaGenerationSize = 20                # Taille d'une génération
IaMutation = 0.06                   # Coefficient de mutation
IaDefaultGenerationTime = 30       # Temps que dure une génération sur un circuit

# Configuration sauvegarde datasetmlp (Non Fonctionnel) ----------------------------------
# Cette fonctionnalité est active uniquement pour le mode IA2. Attendre la 250 ieme génération pour activer cette fonctionalitée
mlpDataSetGeneration = False        # (Ne pas changer)
mlpDataSetFileLeft = 'MlpDataSetLeft.txt'
mlpDataSetFileRight = 'MlpDataSetRight.txt'
mlpDataSetFileForward = 'MlpDataSetForward.txt'
mlpRate = 0.8
mlpEpoch = 10

# Configuration du/des circuit.s ---------------------------------------------------------
# DEFAULT => Charge une piste
# MULTI   => Charge plusieurs circuit
trackMode = 'MULTI'                                         # Mode de circuit (DEFAULT,MULTI) (uniquement pour le mode IA2)
TrackLoadFile = "defaultTrack.txt"                          # Fichier du circuit (DEFAULT)
TrackLoadFileList = ["track1.txt","track2.txt","track3.txt"]# Liste de fichier de circuit (MULTI)

# Configuration des entrées utilisateurs -------------------------------------------------
moveForward = '<Up>'                # Avant
rotateLeft = '<Left>'               # Gauche
rotateRight = '<Right>'             # Droite




