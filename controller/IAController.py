'''
OLD cette classe n'est plus utilisée
'''


from view.CarModel import CarModel
from Config.InterfaceConfig import loadSave, genSelect, simulationSpeed, IaGenerationSize, IaMutation, IaDefaultGenerationTime, carVisible, carSaveFile
from Model.Utils import getMutation
from Model.Car import Car
from operator import attrgetter
import numpy as np
import copy


class IAController:

    carlist = []
    c1 = []

    t, dt, crash, n, generation, bestScore = 0, 0.1, 0, IaGenerationSize, 1, 0

    def __init__(self):
        self.bestCar = []
        open(carSaveFile, 'w').close()


    def randomCarList(self, n, center):
        cars = []
        for i in range(n):
            cars.append(Car(center, 0, i))
            if loadSave :
                self.generation = genSelect
                cars[i].carImport(genSelect)
            else :
                cars[-1].genome = 0.1 * np.random.randn(8)
        return cars

    def deepLearning(self):
        for car in self.carlist:
            if (not car.collide):
                car = self.move(car)
                if (car.collide):
                    self.crash += 1
                else:
                    car.ai()

        if (len(self.carlist) > 0):
            self.t += self.dt
            if (self.t > IaDefaultGenerationTime or self.crash >= IaGenerationSize):
                self.t, self.crash = 0, 0
                best = self.getNextGen()
                if (best > self.bestScore):
                    self.bestScore = best

                self.bestCar[len(self.bestCar) - 1].carExport(self.generation)
                print('Generation(', self.generation, ') -> ', self.bestCar[len(self.bestCar) - 1].distance)
                self.generation += 1


        self.timelabel['text'] = ('Time : ', self.t)
        self.generationLabel['text'] = 'Generation : ', self.generation
        self.bestLabel['text'] = 'Best : ', self.bestScore
        self.fenetre.after(simulationSpeed, self.deepLearning)

    def move(self, car):
        if (car.turn > 0.03):
            self.rightKey(car.id)
        elif (car.turn < 0.03):
            self.leftKey(car.id)
        if (car.power > 1):
            car.power = 1.0
        elif (car.power < -1):
            car.power = -1.0

        car.speed += 30 * car.power * self.dt - 0.02 * car.speed
        if car.speed < 0.0:
            car.speed = 0.0
        self.upKey(car.id)
        car.distance += car.speed*self.dt
        return car

    def getNextGen(self):
        if carVisible:
            for carM in self.c1:
                carM.deleteCar()

        best = sorted(self.carlist, key=attrgetter('distance'))
        best.reverse()

        child = self.ChildGen(IaGenerationSize)
        newCar = []
        self.c1 = []
        id = 0
        for i in child:
            newCar.append(Car(copy.copy(self.track.startPoint),0, id))
            newCar[-1].genome = getMutation(best[i].genome, IaMutation)
            id += 1
        self.carlist = newCar
        self.bestCar.append(best[0])
        if carVisible:
            for car in self.carlist:
                self.c1.append(CarModel(car, self.can))
        return best[0].distance

    def ChildGen(self, gendim):
        listChild = []
        number = 0
        numberMax = 0
        for i in range(0, gendim + 1):
            listChild.append(number)
            listChild.sort()
            if (numberMax == number):
                number = 0
                numberMax += 1
            else:
                number += 1

        return listChild