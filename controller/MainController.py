'''
MainController est le controleur du programme c'est luis qui
orcestre le modele et la vue c'est ici que l'on va definir les circuits parcourus par l'IA
Puis en fonction des éléménts définits dans interfaceConfig la classe vas lancer un mode
'''

#Import
from Config.InterfaceConfig import TrackLoadFileList, TrackLoadFile, interfaceType, carMoveOffset, genSelect, loadTK, carSaveFile, trackMode
from view.MainFont import MainFont
from Model.Track import Track
from Model.Car import Car
from Model.IA.IA1 import IA1
from Model.IA.IA2 import IA2
from Model.IA.IA3 import IA3
from Model.IA.User import User
from view.ConsoleFont import ConsoleFont

class MainController:


    # Constructeur de la classe
    def __init__(self):
        # Creation Liste de Circuit ou circuit solo
        if trackMode == 'MULTI':
            self.trackMultiple = []
            for trackName in TrackLoadFileList:
                self.trackMultiple.append(Track(trackName))
        self.defaultTrack = Track(TrackLoadFile)

        # Voiture par défault (modéle copier pour créé les autres voitures)
        self.carModel = Car(self.defaultTrack.startPoint, carMoveOffset, 0)

        # Mode de controle de la voiture par l'humain
        if (interfaceType == 'USER'):
            print('mode : user')
            self.entity = User()                                    # Chargement de l'entitée utilisateur
            self.entity.loadTrack(self.defaultTrack)                          # Chargement du circuit
            self.entity.loadCar(self.carModel)                      # Chargement de la voiture modele
            self.window = MainFont('User Command', self.entity)     # Création de l'interface de simulation
            self.window.start()                                     # Démarrage de la simulation

        # Mode de controle par l'IA de type "robo moustache"
        elif (interfaceType == 'IA1'):
            print('mode : IA1')
            self.entity = IA1()                                     # Chargement de l'entitée IA1
            self.entity.loadTrack(self.defaultTrack)                          # Chargement du circuit
            self.entity.loadCar(self.carModel)                      # Chargement de la voiture modele
            self.window = MainFont('IA1 Simulate', self.entity)     # Création de l'interface de simulation
            self.window.start()                                     # Démarrage de la simulation

        # Mode de controle par l'IA qui va s'entrainer sur le circuit
        elif (interfaceType == 'IA2'):
            print('mode : IA2')
            self.entity = IA2()                                                     # Chargement de l'entitée IA2
            if trackMode == 'MULTI':
                self.carModel.center = self.trackMultiple[0].startPoint             # Changement centre de la voiture modele
                self.entity.loadTrackMultiple(self.trackMultiple)                   # Chargement de la liste de circuit si le mode "multi circuit" est activé
            else:
                self.entity.loadTrack(self.defaultTrack)                            # Chargement du circuit si le mode "multi circuit" est désactivé
            self.entity.loadCar(self.carModel)                                      # Chargement de la voiture modele
            if loadTK :
                self.window = MainFont('IA2 Simulate Deep Learning', self.entity)   # Création de l'interface de simulation graphique (plus visuel)
                open(carSaveFile, 'w').close()                                      # Vidange du fichier de sauvegarde des générations
                self.window.start()                                                 # Démarrage de la simulation
            else:
                self.window = ConsoleFont(self.entity)                              # Création de l'interface de simulation console (plus performant)
                open(carSaveFile, 'w').close()                                      # Vidange du fichier de sauvegarde des générations
                self.window.start()                                                 # Démarrage de la simulation

        # Mode de controle par l'IA de type multi layer perceptron
        elif (interfaceType == 'IA3'):
            print('mode : IA3')
            self.entity = IA3()                                                     # Chargement de l'entitée IA3
            self.entity.loadTrack(self.defaultTrack)                                # Chargement du circuit si le mode "multi circuit" est désactivé
            self.entity.loadCar(self.carModel)                                      # Chargement de la voiture modele
            self.window = MainFont('IA3 Simulate Machine Learning', self.entity)    # Création de l'interface de simulation graphique (plus visuel)
            open(carSaveFile, 'w').close()                                          # Vidange du fichier de sauvegarde des générations
            self.window.start()                                                     # Démarrage de la simulation

        # Mode qui va charger une génération sur un circuit pour la simuler
        elif (interfaceType == 'LOAD'):
            print('mode : LOAD | Generation : {0}'.format(genSelect))
            self.entity = IA2()                                     # Chargement de l'entitée IA2
            self.entity.loadTrack(self.defaultTrack)                          # Chargement du circuit
            self.entity.loadCar(self.carModel)                      # Chargement de la voiture modele
            self.window = MainFont('IA2 load simulate', self.entity)# Création de l'interface de simulation graphique
            self.window.start()                                     # Démarrage de la simulation
